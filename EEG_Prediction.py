####################################################
# Title   ~ EEG_Prediction
# Authors ~ Josh Griffin
# 			Stephen Covrig 
# 			Abdu Elmaghbub
# Contact ~ griffinj@oregonstate.edu
# Date    ~ 1/19/18 -> future
####################################################

####################################################
# Imports
import pyedflib as edfl
import matplotlib.pyplot as plt
import numpy as np
from os import getcwd, listdir, chdir, mkdir

# Keras Neural Networks
from keras import Sequential
from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Activation, TimeDistributed
from keras.layers import Input, concatenate, Embedding, LSTM, Conv1D
from keras.layers import Flatten
from keras.optimizers import SGD
from keras.models import model_from_json
from keras.utils import np_utils
from keras.preprocessing.sequence import TimeseriesGenerator

# Scikit - learn
# Confusion Matrix
from sklearn.metrics import confusion_matrix
# confusion_matrix(y_true,y_pred)
# C[i,j] - known to be i, but predicted in j
# C[0,0] is True Negatives
from sklearn.preprocessing import MinMaxScaler, OneHotEncoder, LabelBinarizer
# ROC
from sklearn import metrics
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.model_selection import GridSearchCV


# Data manipulation
import pandas as pd
import numpy as np

# Directory info
from os import getcwd, listdir

# Plotting
import matplotlib.pyplot as plt
from matplotlib.pyplot import specgram
from matplotlib.colors import Normalize

# Math
from math import sin, pi, factorial

# Unique Filename timestamps
from time import strftime, strptime, time

# Data Serialization and Loading
from pickle import load, dump

# Scipy Spectrogram
from scipy import signal
from scipy.io import wavfile
from scipy.signal import resample
from scipy.signal import chirp

# Tkinter GUI
from tkinter import Button

# Parallel processing / multicore
import multiprocessing as mp

# Discrete Wavelet Transform
from wavelets import *

# Memory Use Analysis
from sys import getsizeof

# File random order
from random import shuffle
from random import Random

# Memory Cleanup from matplotlib leak
from gc import collect

# Hilbert-Huang Transform
from hilbert import *

import itertools

####################################################


####################################################
# Main class for epilepsy prediction
####################################################
class EEG_Prediction:

# Structure:
	# pre-process
	#	parallel loop through files
	# 		get_session -> normalize -> transform -> save

	# Train
	# 	

# Control Parameters #####
	
	# Directory locations for file operations
	# head = '/run/media/griffinj/SP PHD U3/EP_DEMO/'
	head = '/media/josh/9d2726f1-95f3-4f4f-99dc-3c6859407be0/josh/EP_DEMO/'
	TUH_dir = head + 'NPZ/'
	MIT_dir = head+'Data_In/'
	pickledSessions = head+'PickledData/'
	neuralWeight = head+'Weights/'
	result_directory = head+'Results/'
	testWeight = head+'Weights/mlp_ts_mc_ic_test.h5'

	# Select data source
	data_source = ['TUH','MIT'][1]

	# raw - data in edfs, no preprocessing 
	# partial - pickled, but missing elements
	# ready - preprocessed and transformed, ready for model
	data_readiness = ['raw','partial','ready'][0]

	# Enable model training 
	train_model = ['disable','train','load'][1]
	
	# Enable model testing
	test_model = [False,True][1]

	# Select model for train/test
	model_names = ['mlp','svm','wnn','lstm']
	model_select = ['mlp']

	# Ictal - Preictal classifier
	predictionTypeSet = ['ic','pre']
	predictionTypes = ['ic']

	# Channel multichannel singlechannel select ['sc','mc']
	channelTypeSet = ['sc']

	# Test only a single patient against other channels
	singlePatient = [False,True][0]

	# Time window size for input tensor. 0 -> uses 1 timestep
	# {OUTDATED}
	data_history = 10

	# Number of Channels
	N_Channels = 2

	# Spectrogram
	noverlap	= 500				# Window overlap
	nperseg		= 512 				# Window Size
	spect_in_db = True				# convert to 10log10

	epochs = 1

	# Sample Rate found in data_label/session
	# Computed on runtime for each file 

	# Plotting Enable
	plot_EN = [False,True][0]

	# Classifier Models
	models = []

	# Session counter for filename indexing
	session_ctr = 0

	# Transform Enable/Disable
	timeSeries_EN			= [False,True][0]
	permutationEntropy_EN 	= [False,True][0]
	sampleEntropy_EN 		= [False,True][0]
	wavelet_EN 				= [False,True][0]
	tf_bins_EN				= [False,True][1]
	spectrogram_EN			= [False,True][0]
	hilbert_EN				= [False,True][0]

	# Training and Validation sizes
	num_sessions = 4000
	percent_training = 1
	validationSet = []
	validationSize = 1

	# Only use sessions containing seizures
	seizOnly = [True,False][0]

# Main:
	def __init__(self):
		print('\nEEG Prediction Object Initialized')

	def run(self):
		if(True):

			# self.test_spect()

			# Initialize models
			self.init_models()

			# Initialize data generator
			data = self.generate_data()

			ctr = 0
			validationCTR = 0

			# Get validation set
			if self.test_model:
				# for i in range(int(self.num_sessions*(1-self.percent_training))):
				for i in range(self.validationSize):
					self.validationSet.append(data.__next__())
					ctr += 1

			if self.singlePatient:
				self.trainTestSinglePatient()

			elif self.train_model != 'load' or self.data_readiness != 'ready' and False:
				for session in data:

					# try:

					# Display Results
					if self.plot_EN:
						self.plot_labels(session)

					# Train
					if self.train_model == 'train':

						# Train Model on session
						self.trainModels(session)

					if ctr > self.num_sessions:
						break

					ctr += 1

					del session
					collect()

					# except Exception as e:
					# 	print('Error in main: '+str(e))

			if self.train_model == 'load':
				print('load model')
				self.models[0].load(self.testWeight)
				# self.models[0].model.load_weights[self.testWeight]

			# # Test
			if self.test_model:
				self.testModels()

# Model Training     #####
	def trainModels(self,session):		
		if session.data.shape[0] == self.N_Channels:
			for model in self.models:
				try:
					for i in range(self.epochs):
						model.train_model(session)
				except Exception as e:
					print('Error in training:'+str(e))

		# Remove extra channels
		# if X_train.shape[1] != self.N_Channels:
		# 	X_train = X_train[:,0:self.N_Channels]


		# Y_train = np.array(Y_train)

# Initialize models
	def init_models(self):

		# featureSet = ['ts']
		featureSet = []

		if self.timeSeries_EN:
			featureSet.append('ts')
		if self.permutationEntropy_EN:
			featureSet.append('pe')
		if self.sampleEntropy_EN:
			featureSet.append('se')
		if self.wavelet_EN:
			featureSet.append('dwt')
		if self.spectrogram_EN:
			featureSet.append('sxx')
		if self.tf_bins_EN:
			featureSet.append('bin')
		if self.spectrogram_EN:
			featureSet.append('bin')
		if self.hilbert_EN:
			featureSet.append('hil')

		for Ic_Pre in self.predictionTypes:
			for SC_MC in self.channelTypeSet:
				for C_type in self.model_select:
					for feat in featureSet:
						self.models.append(Classifier(feature=feat,SC_MC=SC_MC,Ic_Pre=Ic_Pre,C_type=C_type,N_Channels=self.N_Channels,fn=self.neuralWeight,rfn=self.result_directory))

# Model Testing      #####
	def testModels(self):
		for model in self.models:
			model.test_model(self.validationSet[0])

# Data Conditioning  #####
	# This function conditions and yields contiguous EEG 
	def generate_data(self):
		
		if self.data_readiness == 'raw':
			if self.data_source == 'TUH':
				data = self.TUH_load_data()
			elif self.data_source == 'MIT':
				data = self.MIT_load_data()
		elif self.data_readiness == 'partial':
			data = self.load_pickles()
		elif self.data_readiness == 'ready':
			data = self.load_pickles()

		# Multiprocess data
		q_in = mp.Queue(mp.cpu_count())
		q_out = mp.Queue(mp.cpu_count())
		q_ctr = mp.Queue()
		pool = mp.Pool(initializer=self.label_transform,initargs=(q_in,q_out,q_ctr))


		ctr2 = 0

		for session in data:

			# Only use files containing seizures
			if 1 in session.label or not self.seizOnly:

				if self.data_readiness == 'ready':
					yield(session)
				else:
					# Parallel processes data
					if self.session_ctr < mp.cpu_count():
						q_ctr.put(self.session_ctr)
						q_in.put(session)
						self.session_ctr += 1
						ctr2 += 1
					else:
						temp_session = q_out.get()
						ctr2 -= 1
						q_ctr.put(self.session_ctr)
						q_in.put(session)
						yield(temp_session)

		if self.session_ctr > mp.cpu_count() or True:
			remaining_processes = mp.cpu_count()
		else:
			remaining_processes = ctr2

		for _ in range(remaining_processes):
			temp_session = q_out.get()
			q_in.put(None)
			yield(temp_session)						

# Data labeling, transform, conditioning for parallel
	def label_transform(self,q_in,q_out,q_ctr):
		
		while True:

			session = q_in.get()

			if session == None:
				break


			# print('+ new session + ')

			# Apply labelling and transforms
			# Label proximity to seizure event
			if session.proximity == []:
				session = self.label_proximity(session)

			# Normalize data 0->1 range
			if session.data.max != 1 and session.data.min != 0:
				session = self.normalize(session)

			# Compute Discrete Wavelet Transform
			if session.wavelets == {} and self.wavelet_EN:
				session = self.wavelet_driver(session)

			# Hilbert-Huang Transform
			if session.hilbert == [] and self.hilbert_EN:
				session = self.get_hilbert_huang(session)

			# Construct time-frequency tensor
			if session.time_frequency == [] and self.spectrogram_EN or self.tf_bins_EN:
				session = self.time_frequency(session)

			# Compute Permutation Entropy
			session.permutation_entropy = []
			if session.permutation_entropy == [] and self.permutationEntropy_EN:
				session = self.get_permutation_entropy(session)

			# Compute Sample Entropy
			session.sample_entropy = []
			if session.sample_entropy == [] and self.sampleEntropy_EN:
				session = self.get_sample_entropy(session)

			# Serialize and save
			if self.data_readiness != 'raw':
				if self.data_readiness == 'partial':
					dump(session,open(session.fn,'wb'))
			else:
				dump(session,open(self.pickledSessions+"TUH_Session_"+strftime("%Y%m%d_%H%M%S_")+str(q_ctr.get())+".p","wb"))

			q_out.put(session)

# Train and test on separate channels of single patient
	def trainTestSinglePatient(self):
		holdout = self.validationSet[0].data[0,:]
		Y_holdout = self.validationSet[0].label
		X_train = self.validationSet[0].data[1:3,:]
		Y_train = self.validationSet[0].label

		# for i in range(self.N_Channels-1):
		for i in range(2):
			self.trainModels(np.transpose(X_train[i,:]),Y_train)
		# self.trainModels(np.transpose(X_train[i,:]),Y_train)

		self.testModels()

# binary label binarizer [0,1] -> [[1,0],[0,1]]
	def bin_lbl_binarizer(self,x):
		lb = LabelBinarizer()
		lb.fit(x)
		y = lb.transform(x)
		y = np.hstack((y,1-y))
		return(y)

# Rescale min->max to 0->1
	def normalize(self,session):
		session.data -= session.data.min()
		session.data = (session.data)/session.data.max()
		return session

# Discrete Wavelet Transform
	def wavelet_driver(self,session):
		npz = {}
		npz['data'] = session.data
		npz['labels'] = np.array(session.label)
		npz['proximity'] = np.array(session.proximity)
		npz_wavelet = get_wavelets(npz,'sym2',7)
		session.wavelets = npz_wavelet['data']
		session.wavelet_label = npz_wavelet['labels']
		session.wavelet_proximity = npz_wavelet['proximity']
		return(session)

# Hilbert-Huang Transform
	def get_hilbert_huang(self,session):
		npz = {}
		npz['data'] = session.data
		npz['labels'] = session.label
		npz['proximity'] = session.proximity
		npz_hilbert = get_hilbert(npz,7)
		session.hilbert = npz_hilbert['data']
		session.hilbert_labels = npz_hilbert['labels']
		session.hilbert_proximity = npz_hilbert['proximity']
		return(session)

# Time Frequency transform of time-series
	def time_frequency(self,session):

		tensor = []

	   	# time between return values: nperseg - noverlap

		# Iterate through Channels
		if len(session.data.shape) == 1:
			channels = 1
		else:
			channels = session.data.shape[0]

		# for i in range(session.data.shape[0]):
		for i in range(channels):

			if len(session.data.shape) == 1:
				f, t, Sxx = signal.spectrogram(session.data,session.samplerate,nperseg = self.nperseg,noverlap=self.noverlap)
			else:
				f, t, Sxx = signal.spectrogram(session.data[i,:],session.samplerate,nperseg = self.nperseg,noverlap=self.noverlap)
			
			Sxx -= Sxx.min()

			Sxx = Sxx / Sxx.max()

			if self.spect_in_db:
				Sxx = 10*np.log10(Sxx)

			tensor.append(Sxx)

		tensor = np.array(tensor)
		tensor[tensor==np.Inf] = 0 
		tensor[tensor==-np.Inf] = 0 

		session.time_frequency = np.array(tensor)
		session.spect_f = f

		def lblGen(x,length,step):
			nps = self.nperseg
			no = self.noverlap
			i = nps-1
			while i < len(x):
				yield(x[i])
				i += (nps-no)

		label_gen = lblGen(session.label,self.nperseg,self.nperseg-self.noverlap)

		session.time_frequency_labels = [lbl for lbl in label_gen]

		session = self.b_wave_bins(session)


		return(session)

# frequency bins alpha beta etc
	def b_wave_bins(self,session):
		sxxd = session.time_frequency
		f = session.spect_f

		# Boundary frequencies for bins
		f_bounds = [.5,4,8,14,30,42,50]
		idx_bounds = [] # Indices for boundaries
		bin_bin = [] # holds bins for each channel

		# Generate boundary indices
		for fr in f_bounds:
			for i in range(len(f)):
				if f[i] == fr:
					idx_bounds.append(i)

		# iterate through channels
		for chn in range(sxxd.shape[0]):
			# Select Channel
			sxx = sxxd[chn,:,:]
			bins = []

			# Sum frequencies in indice range
			for i in range(len(idx_bounds)-1):
				bins.append(sxx[idx_bounds[i]:idx_bounds[i+1],:].sum(axis=0))

			# reshape to original
			bins = np.array(bins)
			bin_bin.append(bins) 

		session.bins = np.array(bin_bin).copy()

		bin_bin = []

		# Normalize
		for chn in range(session.bins.shape[0]):
			n = session.bins[chn]

			for i in range(n.shape[0]):
				n[i,:] -= n[i,:].min()
				n[i,:] = n[i,:]/n[i,:].max()

			# reshape to original
			bins = np.array(n)
			bin_bin.append(bins)



		session.bins = np.array(bin_bin).copy()

		bins = []
		bin_bin = []

		# binarize
		for chn in range(session.bins.shape[0]):
			n = session.bins[chn].copy()

			t = n.mean() + n.var()*2

			n[n>t] = 1
			n[n<=t] = 0

			# reshape to original
			bins = np.array(n)
			bin_bin.append(bins)

		session.bi_bins = np.array(bin_bin)

		return(session)

# Generate time-series tensor - history
	# function assumes collumns are time index
	def generate_timeSeries(self,data,label):
		data = data.transpose()

# Load pickled sessions
	def load_pickles(self):
		pfns = listdir(self.pickledSessions)
		try:
			pfns = listdir(self.good_pickles)
		except:
			print('\n')

		for i in range(len(pfns)):
			fn = pfns[i]
			print(i/len(pfns))
			pickle = load(open(self.pickledSessions + fn,'rb'))
			pickle.fn = self.pickledSessions + fn
			yield(pickle)

# Load MIT .npz files
	def MIT_load_data(self):
		fns = listdir(self.MIT_dir)
		s = data_label()
		for fn in fns:
			x = np.load(self.MIT_dir+fn)
			# s.data = x['data']
			# s.label = x['labels']
			s.data = x['data'][:self.N_Channels,:]
			s.label = x['labels']
			# s.data = x['data'][:self.N_Channels,30000:50000]
			# s.label = x['labels'][30000:50000]
			s.samplerate = 256
			s.fn = self.MIT_dir+fn
			print(s.data.shape)
			yield(s)

# This function traverses the directory and calls 
# new_session on each low-level directory
# Functions on Temple University Dataset
# input: top level directory - must be on level:
# /v1.4.0/edf/train/03_tcp_ar_a
# output: labeled data structure
	def TUH_load_data(self):

		sourceDir = self.TUH_dir

		# Working directory for future reference
		current_wd = sourceDir
		cd = lambda wd,d: wd + '/' + d
		bd = lambda wd: ''.join(['/'+l for l in wd.split('/')[0:-1]])[1:]

		# Identifiers: Patient IDs
		identifiers = listdir(sourceDir)

		# Initialize data to empty list 
		# Include data sample rate and other data
		# data = []

		# initialize
		edfns = []
		tse_bi_fns = []
		cwds = []


		# Index partitions of data
		for identifier in identifiers:

			current_wd = cd(current_wd,identifier)
			# get list of patients IDs and index
			patients = listdir(current_wd)

			for patient in patients:

				current_wd = cd(current_wd,patient)
				# Get list of sessions and index
				sessions = listdir(current_wd)
				for session in sessions:

					# start at t000 (session 0)
					# then increment (t001)
					# If there is a gap, keep them separate
					current_wd = cd(current_wd,session)
					# all filenames
					fns = listdir(current_wd)
					# all edf files
					edfns.append([fn for fn in fns if '.edf' in fn])
					# binary tse files
					tse_bi_fns.append([fn for fn in fns if '.tse_bi' in fn])
					# corresponding working directories:
					cwds.append(current_wd)


					current_wd = bd(current_wd)
				current_wd = bd(current_wd)
			current_wd = bd(current_wd)
		current_wd = bd(current_wd)

		ls = list(zip(edfns,tse_bi_fns,cwds))
		shuffle(ls)
		edfns,tse_bi_fns,cwds = zip(*ls)
		ctr = 0
		for xedfns,xtse_bi_fns,xcurrent_wd in zip(edfns,tse_bi_fns,cwds):
			# if not empty, append data and get labels
			if xedfns:
				# print(xcurrent_wd)
				gen_session = self.new_session(xedfns,xtse_bi_fns,xcurrent_wd)
				for s in gen_session:
					print(ctr/len(edfns))
					ctr+=1
					yield(s)

# Yield contiguous data_label session
	def new_session(self,edfns,tse_bi_fns,cwd):

		# sort the list
		# this ensures start = t000, end = max tNNN
		edfns.sort()
		tse_bi_fns.sort()

		# pair matching edf files with label files
		fns = zip(edfns,tse_bi_fns)

		# Initialize new session
		# data.append(data_label())

		#  initial time counter
		t = '000'

		# a flip flop bool to know if we need a reset
		initialize_new_data = True

		# append new file data
		for fn in fns:

			# load data from file (edf)
			edf_data = edfl.EdfReader(cwd + '/' + fn[0])
			n = edf_data.signals_in_file
			NSamples = edf_data.getNSamples()[0]

			# initialize data-to-append
			if initialize_new_data:

				# initial data
				new_data = data_label()
				new_data.data = np.zeros([n, NSamples])
				new_data.label = []
				initialize_new_data = False
				for i in np.arange(n):
					new_data.data[i,:] = edf_data.readSignal(i)

				# initial label
				ts_lbl = self.load_tse_bi(fn[1],cwd)

				# get samplerate: #samples/totalTime : should be 400
				samplerate = new_data.data.shape[1]/float(ts_lbl[len(ts_lbl)-1][1])

				new_data.samplerate = samplerate

				# index through sets of contiguous label
				# get index by mulitplying min,max ts with samplerate
				for lbl in ts_lbl:
					for i in range(int(samplerate*float(lbl[0])),int(samplerate*float(lbl[1]))):
						if lbl[2] == 'bckg':
							new_data.label.append(0)
						else:
							new_data.label.append(1)
						# new_data.label.append(lbl[2])

			# Append new data to existing temp array
			# This concatenates contiguous time series
			else:

				# append data
				temp_data = data_label()
				temp_data.data = np.zeros([n, NSamples])
				temp_data.label = []
				for i in np.arange(n):
					temp_data.data[i,:] = edf_data.readSignal(i)

				new_data.data = np.append(new_data.data,temp_data.data,axis=1)

				# initial label
				ts_lbl = self.load_tse_bi(fn[1],cwd)

				# get samplerate: #samples/totalTime : should be 400
				samplerate = temp_data.data.shape[1]/float(ts_lbl[len(ts_lbl)-1][1])
			
				if samplerate != new_data.samplerate:
					print(['Samplerate mismatch',samplerate,new_data.samplerate])

				# index through sets of contiguous label
				# get index by mulitplying min,max ts with samplerate
				for lbl in ts_lbl:
					for i in range(int(samplerate*float(lbl[0])),int(samplerate*float(lbl[1]))):
						if lbl[2] == 'bckg':
							new_data.label.append(0)
						else:
							new_data.label.append(1)

			# Ensure that files are contiguous t000, t001, t002 ...
			if 't'+t in fn[0]:

				# increment t
				temp = str(int(t)+1)
				l = len(temp)
				for i in range(0,3-l):
					temp = '0'+temp
				t = temp

			else:
				# append new_data to data
				yield(new_data)

				# new list
				new_data = data_label()

				# init new_data
				initialize_new_data = True

				# catch up to the next edf tNNN
				while('t'+t not in fn[0]):
					temp = str(int(t)+1)
					l = len(temp)
					for i in range(0,3-l):
						temp = '0'+temp
					t = temp

		if not initialize_new_data:
			# append new_data to data
			yield(new_data)
		
		# init new_data
		initialize_new_data = True

# label time until seizure event
	def label_proximity(self,session):

		session.proximity = []
		trigger = False
		ctr = 0
		# for lbl in session.label:
		for i in reversed(range(len(session.label))):
			lbl = session.label[i]
			if lbl == 1:
				trigger = True
				ctr = 0
				session.proximity.append(0)
			else:
				if not trigger:
					session.proximity.append(-1)
				else:
					ctr+=1
					session.proximity.append(ctr / session.samplerate)

		session.proximity.reverse()

		return(session)

# Input: .tse_bi ; binary seizure label
# Output: start times of bckg, seiz
	def load_tse_bi(self,fn,cwd):
		f = open(cwd+'/'+fn)

		lines = []

		for line in f:
			lines.append(line)

		lines = lines[2:]

		ts_lbl = []

		for line in lines:
			ts_lbl.append(line.split())
			ts_lbl[len(ts_lbl)-1] = ts_lbl[len(ts_lbl)-1][:-1]

		return ts_lbl

# Get Spectrogram - mode select (complex,phase,magnitude)
	def getSxx(self,fn,Mode):

		Fs,x = wavfile.read(fn)
		chnCNT = len(x.shape)
		if chnCNT == 2:
			x = x.sum(axis=1)/2

		x = resample(x,int(np.floor(len(x)*400/44100)))
		Fs = 400

	   	# time between return values: nperseg - noverlap
	   	# target = Ts
		nps = self.nperseg
		nov = self.noverlap
		f, t, Sxx = signal.spectrogram(x,Fs,nperseg = nps,noverlap=nov,mode=Mode)
		return(Sxx)

# Compute Permutation entropy
	def get_permutation_entropy(self,session):
		# Iterate channels
		for channel in range(session.data.shape[0]):
			# Generate Time series
			length = 1000
			stride = 1000
			time_series_sets = TimeseriesGenerator(session.data[channel,:],session.data[channel,:],length,stride=stride,batch_size=int(session.data.shape[1]/length))[0][0]

			# Compute PE
			session.permutation_entropy.append(self.permutation_entropy(time_series_sets,3,1,True))

		# convert list of channels to np.array of channels
		session.permutation_entropy = np.array(session.permutation_entropy)

		# Select labels at end of window
		session.permutation_entropy_labels = TimeseriesGenerator(np.array(session.label),np.array(session.label),length,stride=stride,batch_size=int(session.data.shape[1]/length))[0][0]
		session.permutation_entropy_labels = list(session.permutation_entropy_labels[:,-1])

		return(session)

	"""----------------------------------------------------------------------------------------------------------------------------------
	#Function Description: This function create the symbols(sequences) of the time series with its length equlas to the
	permutation order and its step equals to the time delay. Then it puts all the symbols together in a nD matrix for further processing.

	#Function Inpust: time series with N values(1D array), order of permutation(int), and embedding delayself(int).

	#Function Output: sequence matrix (nD array) that has ordinal sequences of the time series with size of (step, permutation order)
	----------------------------------------------------------------------------------------------------------------------------------"""
	def sequencing(self,x, order=3, delay=1):
	    # find the length of the window
	    N = len(x)
	    # Step is the number of column vectors created.
	    step = N - (order - 1) * delay
	    # allocate number of rows and columns for the sequence matrix
	    S = np.empty((step,order))
	    # creating the sequences of of size (1,order)
	    for i in range(step):
	        S[i] = x[i * delay:i * delay + order]
	    # return the sequence matrix ( step,order)
	    return S
	"""------------------------------------------------------------------------------------------------------------------------------
	#Function name:Permutation_entropy.
	#Function Description:This funciton would calculate the permutation entropy of a given time series (array(int)) with an option of
	calculating its normalized value.
	#Function Input:time series (1D array(int) or list), order of permutation (int), Embedding delay(int), and normaliztion option (bool)
	#Function Output:the total permutation entropy of a given time series (float) in bits. If the normalization option chosen to be True,
	the ouput will between 0 and 1.
	----------------------------------------------------------------------------------------------------------------------------------"""
	def permutation_entropy(self,time_series, order, delay, normalize):
	    # converting the list to an array
	    y = []
	    for channel in range(time_series.shape[0]):

	        x = time_series[channel,:]

	        # create the symbols of x and sort the order of permutations
	        sorted_idx = self.sequencing(x, order=order, delay=delay).argsort(kind='mergesort')

	        hashmult = np.power(order, np.arange(order))
	        # Associate unique integer to each permutations
	        hashval = (np.multiply(sorted_idx, hashmult))
	        # Return the counts
	        _, counts = np.unique(hashval.sum(1), return_counts=True)
	        # find the relative frequency (probability) of all ocurred symbols.
	        # for Python 2 either import division from future or use the compatible np.true_divide()
	        p = counts / counts.sum(0)
	        # find the total entropy which is the negative summation of all symbols probabilities multipied by its logarithm of base 2.
	        pe = -np.multiply(p, np.log2(p)).sum(0)
	        # if the normalize argument is true, divide the total entropy by the highest possible value log2(order!)
	        if normalize:
	            pe /= np.log2(factorial(order))
	        y.append(pe)

	    #print(y)
	    return y

# Compute Sample entropy
	def get_sample_entropy(self,session):
		# Iterate channels
		for channel in range(session.data.shape[0]):
			# Generate Time series
			length = 1000
			stride = 1000
			time_series_sets = TimeseriesGenerator(session.data[channel,:],session.data[channel,:],length,stride=stride,batch_size=int(session.data.shape[1]/length))[0][0]

			# Compute PE
			session.sample_entropy.append(self.sample_entropy(time_series_sets))

		# convert list of channels to np.array of channels
		session.sample_entropy = np.array(session.sample_entropy)

		# Select labels at end of window
		session.sample_entropy_labels = TimeseriesGenerator(np.array(session.label),np.array(session.label),length,stride=stride,batch_size=int(session.data.shape[1]/length))[0][0]
		session.sample_entropy_labels = list(session.sample_entropy_labels[:,-1])

		return(session)

	# ts - time series
	# m - window length
	# r - tolerance
	def sample_entropy(self,ts, m=3, r=.2):
	    response = []

	    for window in range(ts.shape[0]):
	        L = np.array(ts[window])

	        N = len(L)
	        B = 0.0
	        A = 0.0

	        # Split time series and save all templates of length m
	        xmi = np.array([L[i:i+m] for i in range(N-m)])
	        xmj = np.array([L[i:i+m] for i in range(N-m+1)])

	        # Save all matches minus the self-match, compute B
	        B = np.sum([np.sum(np.abs(xmii-xmj).max(axis=1) <= r)-1 for xmii in xmi])
	        # print(B)
	        # Similar for computing A
	        m += 1
	        xm = np.array([L[i:i+m] for i in range(N-m+1)])

	        A = np.sum([np.sum(np.abs(xmi-xm).max(axis=1) <= r)-1 for xmi in xm])
	        # print(A)
	        # Return SampEn
	        if B == 0 or A == 0 or (A/B) > 2.72:
	            response.append(1)
	        else:
	            response.append(-np.log(A/B))

	    # 1D series, list
	    return response

# Test Spectrogram with sin sweep
	def test_spect(self):
		x = data_label()
		fs = 400
		x.samplerate = fs
		T = 100
		t = np.linspace(0,T,T*fs,endpoint=False)
		x.data = chirp(t,f0=1,f1=200,t1=T)
		x = self.time_frequency(x)

		plt.subplot(2,1,1)
		plt.imshow(x.time_frequency[0,:,:])

		plt.subplot(2,1,2)
		plt.plot(x.data)
		plt.show()

# GUI initialization
	def init_gui(self):
		print('temp')

# Cross Correlation / AutoCorrelation
	def crossCorrelation(self,s1,s2):
		cc = []
		z1 = list(np.zeros(len(s1)))
		z2 = list(np.zeros(len(s2)))


		# for n in range(len(s2)):

# Plot list of label values
	def plot_labels(self,session):

		num_active = sum([self.permutationEntropy_EN,self.sampleEntropy_EN,self.spectrogram_EN,self.hilbert_EN])
		# num_active += 2

		plot_idx = 1

		# Permutation Entropy
		if self.hilbert_EN:
			print('Hilbert-Huang shape: '+str(session.hilbert.shape))
			plt.subplot(num_active,1,plot_idx)
			
			plt.plot(session.hilbert[0,:,:])
			# plt.plot(np.mean(session.permutation_entropy,axis=0))
			plot_idx += 1


		# Permutation Entropy
		if self.permutationEntropy_EN:
			print('permutation entropy shape: '+str(session.permutation_entropy.shape))
			plt.subplot(num_active,1,plot_idx)
			
			plt.plot(np.mean(session.permutation_entropy,axis=0))
			# plt.plot([i for i in range(session.permutation_entropy.shape[1])],session.permutation_entropy[0,:])
			# plt.plot([i for i in range(session.permutation_entropy.shape[1])],session.permutation_entropy[1,:])
			# plt.plot([i for i in range(session.permutation_entropy.shape[1])],session.permutation_entropy[2,:])
			plot_idx += 1

		# sample Entropy
		if self.sampleEntropy_EN:
			print(len(session.sample_entropy))
			print('sample entropy shape: '+str(session.sample_entropy.shape))
			plt.subplot(num_active,1,plot_idx)
			
			plt.plot(np.mean(session.sample_entropy,axis=0))
			# plt.plot([i for i in range(session.sample_entropy.shape[1])],session.sample_entropy[0,:])
			# plt.plot([i for i in range(session.sample_entropy.shape[1])],session.sample_entropy[1,:])
			# plt.plot([i for i in range(session.sample_entropy.shape[1])],session.sample_entropy[2,:])
			plot_idx += 1	

		# Spectrogram
		if self.spectrogram_EN:
			print('Spectrogram Shape: '+str(session.time_frequency.shape))
			plt.subplot(num_active,1,plot_idx)
			plt.imshow(session.time_frequency[0,:,:],aspect='auto')
			# plt.imshow(np.mean(session.time_frequency,axis=0))
			plot_idx += 1

		# Summed frequency bands
		if self.tf_bins_EN:
			# print('TF Bin Shape: '+str(session.bi_bins.shape))
			print('TF Bin Shape: '+str(session.bins.shape))
			plt.figure('Time Frequency Bins')
			# n = session.bi_bins[0]
			n = session.bins[0]
			lbl = np.array(session.time_frequency_labels)
			lbl = lbl[None,:]
			lbl -= lbl.min()
			lbl = lbl/lbl.max()
			n = np.append(n,lbl,axis=0)
			plt.imshow(n,aspect='auto',cmap='plasma')
						
		if self.timeSeries_EN:
			plt.figure('Data and Label')
			# Data
			plt.subplot(num_active,1,plot_idx)
			plt.plot(np.mean(session.data,axis=0))		
			# plt.plot(session.data[0,:])
			# plt.plot(session.data[1,:])
			# plt.plot(session.data[2,:])
			plot_idx += 1

			# Proximity and label
			plt.subplot(num_active,1,plot_idx)
			x = range(0,len(session.label))
			plt.plot(x,session.label)
			plt.plot(x,session.proximity)
			plot_idx += 1

		# Discrete Wavelet Transform
		if self.wavelet_EN:
			npz = {}
			npz['data'] = session.wavelets
			npz['labels'] = np.array(session.wavelet_label)
			npz['proximity'] = np.array(session.wavelet_proximity)
			print_wavelets_norm(npz,0)

		plt.show()

####################################################
# This class stores data and corresponding labels
# This data structure is serialized, and saved
####################################################
class data_label:
	# True Labels
	label = []
	proximity = []
	data_temp = []
	prediction = []

	data = np.zeros([1,1])
	samplerate = 0

	fn = ''

	# Spectrogram Data
	time_frequency 	= []
	spect_f			= []
	bins 			= []	
	bi_bins			= []
	time_frequency_labels = []
	tf_prediction 	= []


	# Permutation entropy
	permutation_entropy = []
	permutation_entropy_labels = []
	pe_prediction = []

	# Sample entropy
	sample_entropy = []
	sample_entropy_labels = []

	# Discrete Wavelet Transform
	wavelets = {}
	wavelet_label = []
	wavelet_proximity = []

	# Hilbert-Huang Transform
	hilbert = []
	hilbert_labels = []
	hilbert_proximity = []

# Stores classification metrics
class results:
	cfm = []
	tpr = []
	fpr = []
	threshold = []
	auc = []
	report = ''
	real = []
	prediction = []

	def displayResults(self):
		tpr = self.tpr
		fpr = self.fpr

		print('Confusion Matrix:')
		plt.figure(1)
		plot_confusion_matrix(self.cfm,normalize=True,classes=['True','False'])
		print('Sensitivity:'+str(tpr[int(len(tpr)/2)]))
		print('Specificity:'+str(1-fpr[int(len(fpr)/2)]))
		print('Area Under ROC:')
		print(self.auc)
		print(self.report)
		plt.figure("Predicted and Real Labels")
		# plt.
		plt.figure(2)
		self.plotROC()

				

	def plotROC(self):
		plt.plot(self.fpr,self.tpr)
		plt.show()

# classifier class
class Classifier:

	feature = ''	# Target feature
	SC_MC	= ''	# Single or MultiChannel
	Ic_Pre	= ''	# Ictal or Pre-ictal
	C_type	= ''	# Classifier Type
	C_res	= ''	# Classifier results
	in_dim 	= ''	# Dimension of input data
	model 	= ''	# Classifier model object

	downsample = 20
	neuralWeight = ''
	randomSeed = 2

	def __init__(self,feature,SC_MC,Ic_Pre,C_type,N_Channels,fn,rfn):
		self.feature	=	feature
		self.SC_MC		=	SC_MC
		self.Ic_Pre		=	Ic_Pre
		self.C_type		=	C_type
		self.N_Channels = 	N_Channels

		# Save locations
		self.neuralWeight = fn
		self.result_directory = rfn

		# [timeWindow,width]
		featureDim = {}
		featureDim['se'] = [4,1]
		featureDim['pe'] = [4,1]
		featureDim['ts'] = [2*256,1]
		# featureDim['ts'] = [512,1]
		# featureDim['ts'] = [16,1]
		featureDim['sxx'] = [10,257]
		# featureDim['bin'] = [512,6]
		featureDim['bin'] = [64,6]
		featureDim['dwt'] = [512,7]
		featureDim['hil'] = [512,7]

		self.featureDim = featureDim

		self.init_model()

# Init models
	def init_model(self):
		if self.C_type == 'mlp':

			# MLP Model
			model = Sequential()

			# Layers
			if self.SC_MC == 'mc':
				model.add(Dense(100, activation='relu', input_dim=self.N_Channels*np.prod(self.featureDim[self.feature])))
			else:
				model.add(Dense(512, activation='relu', input_dim=np.prod(self.featureDim[self.feature])))
				# model.add(Dense(3072, activation='relu', input_dim=np.prod(self.featureDim[self.feature])))
			model.add(Dropout(0.75))
			# model.add(Dense(1024, activation='relu'))
			# model.add(Dropout(0.75))
			model.add(Dense(256, activation='relu'))
			model.add(Dropout(0.75))
			# model.add(Dense(128, activation='relu'))
			# model.add(Dropout(0.75))
			# model.add(Dense(64, activation='relu'))
			# model.add(Dropout(0.75))
			# model.add(Dense(32, activation='relu'))
			# model.add(Dropout(0.75))
			# model.add(Dense(16, activatoin='relu'))
			# model.add(Dropout(0.75))
			model.add(Dense(64, activation='relu'))
			model.add(Dropout(0.75))
			model.add(Dense(16, activation='relu'))
			model.add(Dropout(0.75))
			model.add(Dense(2, activation='softmax'))

			# Optimizer
			# sgd = SGD(lr=0.001, decay=0, momentum=0, nesterov=True)
			
			# sgd = SGD()

			# Compile model
			model.compile(loss='categorical_crossentropy',
			              optimizer='adagrad')

			self.model = model


		elif self.C_type == 'svm':
			#tunning the C and gamma parameters using searchgridCV
			C_range = np.logspace(-2, 10, 13)
			gamma_range = np.logspace(-9, 3, 13)
			param_grid = dict(gamma=gamma_range, C=C_range)


			#choose the Kernel type
			cv = StratifiedShuffleSplit(n_splits=5, test_size=0.2, random_state=42)
			svmClassifier = GridSearchCV(SVC(kernel = 'rbf',probability = True), param_grid = param_grid, cv=cv)

			self.model = svmClassifier


		elif self.C_type == 'wnn':
			print('temp')

		elif self.C_type == 'lstm':
			# print('temp')
			lm = Sequential()
			if self.SC_MC == 'sc':
				lm.add(LSTM(32,input_shape=(self.featureDim[self.feature][0],self.featureDim[self.feature][1]), return_sequences=True))
			elif self.SC_MC == 'mc':
				lm.add(LSTM(32,input_shape=(1,self.featureDim[self.feature][0],self.N_Channels), return_sequences=True))

			lm.add(LSTM(32, return_sequences=True))
			lm.add(Dropout(0.5))
			lm.add(LSTM(32, return_sequences=True))
			lm.add(Dropout(0.5))
			lm.add(LSTM(32, return_sequences=True))
			lm.add(Dropout(0.5))
			lm.add(Flatten())
			lm.add(Dense(512))
			lm.add(Dropout(0.5))
			lm.add(Dense(32))
			lm.add(Dropout(0.5))
			lm.add(Dense(2))
			model.add(Activation('linear'))
			lm.compile(loss='categorical_crossentropy', optimizer='adam')
			self.model = lm

# Train Model
	def train_model(self,session):
		
		if self.feature == 'ts':
			X_train = np.swapaxes(session.data,0,1)
			Y_train = session.label
		elif self.feature == 'se':
			X_train = np.swapaxes(session.sample_entropy,0,1)
			Y_train = np.array(session.sample_entropy_labels)
		elif self.feature == 'pe':
			X_train = np.swapaxes(session.permutation_entropy,0,1)
			Y_train = np.array(session.permutation_entropy_labels)
		elif self.feature == 'sxx':
			X_train = np.swapaxes(np.swapaxes(session.time_frequency,0,2),1,2)
			Y_train = np.array(session.time_frequency_labels)
		elif self.feature == 'bin':
			X_train = np.swapaxes(np.swapaxes(session.bins,0,2),1,2)
			# X_train = np.swapaxes(np.swapaxes(session.bi_bins,0,2),1,2)
			Y_train = np.array(session.time_frequency_labels)
		elif self.feature == 'dwt':
			X_train = np.swapaxes(session.wavelets,0,1)
			Y_train = session.wavelet_label
		elif self.feature == 'hil':
			X_train = np.swapaxes(session.hilbert,0,1)
			Y_train = session.hilbert_labels

		X_train -= X_train.min()
		X_train = (X_train)/X_train.max()


		ensemble_num = 1
		epochs = 1 # number of epochs (complete training episodes over the training set) to run
		batch = 1 # mini batch size for better convergence

		# Time series generator for windowed classification
		stride = 1

		length = self.featureDim[self.feature][0]


		print('\nTraining Model with Config: ')
		print('  Ictal/Pre-ictal: '+self.Ic_Pre)
		print('  Single/Multi-Channel: '+self.SC_MC)
		print('  Feature Type: '+self.feature)
		print('  Model Type: '+self.C_type)
		print('')

		# Steps per epoch
		spe = X_train.shape[0]-length
		if self.feature in ['ts','bin']:
			spe = int(spe/self.downsample)

		if self.SC_MC == 'sc':
			for chn in range(self.N_Channels):
				# xgen = self.windowGen(np.take(X_train,chn,axis=1),length)
				xgen = self.windowGen(X_train[:,chn],length)
				ygen = self.windowGen(Y_train,length)
				if self.C_type != 'lstm':
					gen = self.xygen(xgen,ygen,ensemble_num)
				else:
					gen = self.lstmGen(xgen,ygen)
				if self.C_type == 'mlp':
					self.model.fit_generator(gen,steps_per_epoch=spe,epochs=epochs)
				elif self.C_type == 'lstm':
					self.model.fit_generator(gen,steps_per_epoch=spe,epochs=epochs)
				elif self.C_type == 'svm':
					xs,ys = gen.__next__()
					for x,y in gen:
						xs = np.append(xs,x,axis=0)
						ys = np.append(ys,y,axis=0)
					print(xs.shape)
					self.model.fit(xs,ys[:,1])

		else:
			xgen = self.windowGen(X_train,length)
			ygen = self.windowGen(Y_train,length)
			if self.C_type != 'lstm':
				gen = self.xygen(xgen,ygen,ensemble_num)
			else:
				gen = self.lstmGen(xgen,ygen)
			if self.C_type == 'mlp':
				self.model.fit_generator(gen,steps_per_epoch=spe,epochs=epochs)
			elif self.C_type == 'lstm':
				self.model.fit_generator(gen,steps_per_epoch=spe,epochs=epochs)
			elif self.C_type == 'svm':
				xs,ys = gen.__next__()
				for x,y in gen:
					xs = np.append(xs,x,axis=0)
					ys = np.append(ys,y,axis=0)
				print(xs.shape)
				self.model.fit(xs,ys[:,1])

		# vxgen = self.windowGen(self.validationSet[0].data[0,:],length)
		# vygen = self.windowGen(self.validationSet[0].label,length)
		# validation_gen = self.xygen(vxgen,vygen,ensemble_num)

		# self.models['mlp'].fit_generator(gen,steps_per_epoch=len(X_train)-length,epochs=epochs,validation_data=validation_gen,nb_val_samples=len(self.validationSet[0].data[0,:])-length)
		# self.session_ctr += 1
		# self.saveModel()

# Test model
	def test_model(self,session):
		

		self.saveModel()

		print('\nTesting Model with Config: ')
		print('  Ictal/Pre-ictal: '+self.Ic_Pre)
		print('  Single/Multi-Channel: '+self.SC_MC)
		print('  Feature Type: '+self.feature)
		print('  Model Type: '+self.C_type)
		print('  Test data length(sec): ' + str(session.data.shape[1]/session.samplerate))

		start_t = time()
		print('  Start Time: ' + strftime("%H:%M:%S"))

		length = self.featureDim[self.feature][0]

		# init result object
		res = results()

		def endofY(yin):
			for y in yin:
				yield(np.array([np.array([1-y[-1],y[-1]])]))

		def xTransGen(xin):
			for x in xin:
				yield(np.array([np.transpose(x)]))

		# initialize predictions and real labels
		ypredict = []
		yreal = []

		# alias model
		model = self.model

		if self.feature == 'ts':
			X_train = np.swapaxes(session.data,0,1)
			Y_train = session.label
		elif self.feature == 'se':
			X_train = np.swapaxes(session.sample_entropy,0,1)
			Y_train = np.array(session.sample_entropy_labels)
		elif self.feature == 'pe':
			X_train = np.swapaxes(session.permutation_entropy,0,1)
			Y_train = np.array(session.permutation_entropy_labels)
		elif self.feature == 'sxx':
			X_train = np.swapaxes(np.swapaxes(session.time_frequency,0,2),1,2)
			Y_train = np.array(session.time_frequency_labels)
		elif self.feature == 'bin':
			X_train = np.swapaxes(np.swapaxes(session.bins,0,2),1,2)
			# X_train = np.swapaxes(np.swapaxes(session.bi_bins,0,2),1,2)
			Y_train = np.array(session.time_frequency_labels)
		elif self.feature == 'dwt':
			X_train = np.swapaxes(session.wavelets,0,1)
			Y_train = session.wavelet_label
		elif self.feature == 'hil':
			X_train = np.swapaxes(session.hilbert,0,1)
			Y_train = session.hilbert_labels

		X_train -= X_train.min()
		X_train = (X_train)/X_train.max()


		# Steps per epoch
		spe = X_train.shape[0]-length

		ensemble_num = 1

		if self.SC_MC == 'sc':
			for chn in range(self.N_Channels):
				# xgen = self.windowGen(np.take(X_train,chn,axis=1),length)
				xgen = self.windowGen(X_train[:,chn],length)
				ygen = self.windowGen(Y_train,length)
				if self.C_type != 'lstm':
					gen = self.xygen(xgen,ygen,ensemble_num)
				else:
					gen = self.lstmGen(xgen,ygen)

				xg = self.xunzip(gen)
				yg = self.yunzip(gen)
				if self.C_type in ['mlp','lstm']:
				
					ypredict.append(model.predict_generator(xg,steps=spe))
					xgen = self.windowGen(X_train[:,chn],length)
					ygen = self.windowGen(Y_train,length)
					if self.C_type != 'lstm':
						gen = self.xygen(xgen,ygen,ensemble_num)
					else:
						gen = self.lstmGen(xgen,ygen)
					xg = self.xunzip(gen)
					yg = self.yunzip(gen)
					yreal.append([lbl for lbl in yg])

				elif self.C_type == 'svm':
					xs,ys = gen.__next__()
					for x,y in gen:
						xs = np.append(xs,x,axis=0)
						ys = np.append(ys,y,axis=0)
					yp = self.model.predict_proba(xs)



		else:
			xgen = self.windowGen(X_train,length,shuffleEN=False)
			ygen = self.windowGen(Y_train,length,shuffleEN=False)
			gen = self.xygen(xgen,ygen,ensemble_num)
			xg = self.xunzip(gen)
			yg = self.yunzip(gen)
			if self.C_type == 'mlp':

					ypredict.append(model.predict_generator(xg,steps=spe))
					xgen = self.windowGen(X_train,length,shuffleEN=False)
					ygen = self.windowGen(Y_train,length,shuffleEN=False)
					gen = self.xygen(xgen,ygen,ensemble_num)
					xg = self.xunzip(gen)
					yg = self.yunzip(gen)
					yreal.append([lbl for lbl in yg])

			elif self.C_type == 'svm':
				xs,ys = gen.__next__()
				for x,y in gen:
					xs = np.append(xs,x,axis=0)
					ys = np.append(ys,y,axis=0)
				yp = self.model.predict_proba(xs)

		if self.C_type in ['mlp','lstm']:
			yp = np.concatenate(ypredict,axis=1)
			yr = np.concatenate(yreal,axis=1)
			yr = yr[:,0,:]

		# print(yr.shape)
		# yp [:,1] = session.bins[0,0,:yr.shape[0]]
		yp_binary = np.copy(yp[:,1])
		t = yp_binary.mean()-2*yp_binary.var()
		yp_binary[yp_binary>t] = 1
		yp_binary[yp_binary<=t] = 0
		# yp_binary[yp_binary>.5] = 1
		# yp_binary[yp_binary<=.5] = 0

		yr_binary = yr[:,1]

		res.cfm = confusion_matrix(yr_binary,yp_binary)
		# res.cfm = res.cfm/res.cfm.astype(np.float).sum(axis=1)

		res.fpr, res.tpr, res.thresholds = metrics.roc_curve(yr[:,1],yp[:,1],pos_label=1)
		res.auc = metrics.auc(res.fpr,res.tpr)
		
		res.report = metrics.classification_report(yr_binary,yp_binary)

		stop_t = time()
		print('  Stop Time: ' + strftime("%H:%M:%S"))
		run_t = stop_t-start_t
		print('  Compute Time: ' + str(run_t))
		print('')
		res.displayResults()

		dump(res,open(self.result_directory+'results_'+strftime("%Y%m%d-%H%M%S")+".p",'wb'))		

		# for session in vsession:
		# 	# for i in range(self.N_Channels):
		# for i in range(1):
		# 	xgen = self.windowGen(session.data[i,:],self.data_history)
		# 	ygen = self.windowGen(session.label,self.data_history)
		# 	yend = endofY(ygen)
		# 	xtrans = xTransGen(xgen)

		# 	ypredict.append(mlp.predict_generator(xtrans,steps=len(session.data[i,:])-self.data_history))
		# 	yreal.append([lbl for lbl in yend])

# Sliding window N-Dimensional generator, along axis 0
	def windowGen(self,x,length,shuffleEN=True):
		l = x.shape[0]

		idxs = [i for i in range(l-length)]
		if shuffleEN:
			Random(1).shuffle(idxs)

		for i in idxs:
			idx = [ix for ix in range(i,i+length)]
			yield(np.take(x,idx,axis=0))

# Generator Zipper 1,1 -> (1,1)
	def xygen(self,xg,yg,ensNum):
		for x,y in zip(xg,yg):
			xout = np.array(np.reshape([x],-1))
			xout = np.array(xout[None,:])
			yout = np.array([np.array([1-y[-1],y[-1]])])

			if ensNum == 1:
				yield(xout,yout)
			else:
				yield([xout]*ensNum,yout)

# Lstm generator
	def lstmGen(self,xg,yg):
		for x,y in zip(xg,yg):
			# Shape format: [batch size, time steps, feature len]
			xout = x.reshape(1,self.featureDim[self.feature][0],self.featureDim[self.feature][1])
			yout = np.array([np.array([1-y[-1],y[-1]])])
			yield(xout,yout)

# xunzip
	def xunzip(self,xygen):
		for x,y in xygen:
			yield(x)

# yunzip
	def yunzip(self,xygen):
		for x,y in xygen:
			yield(y)

# load and save
	def load(self,WeightFn):
		self.model.load_weights(WeightFn)

	# Save model and weights 
	def saveModel(self):

			# serialize weights to HDF5			
			if self.C_type == 'mlp':
				self.model.save_weights(self.neuralWeight+self.C_type+'_'+self.feature+'_'+self.SC_MC+'_'+self.Ic_Pre+'_'+'.h5')
				print('model saved to disk')

def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.tight_layout()

if __name__ == '__main__':
	print('Main')
	EEG = EEG_Prediction()
	EEG.run()

###########
# Bug Log #
###########
# errors when permutation entropy is not reinitialized 187


