# -*- coding: utf-8 -*-
import numpy as np

# ts - time series
# m - window length
# r - tolerance
def sampen(ts, m, r):
    response = []

    for window in range(len(ts)):
        L = np.array(ts[window])

        N = len(L)
        B = 0.0
        A = 0.0

        # Split time series and save all templates of length m
        xmi = np.array([L[i:i+m] for i in range(N-m)])
        xmj = np.array([L[i:i+m] for i in range(N-m+1)])

        # Save all matches minus the self-match, compute B
        B = np.sum([np.sum(np.abs(xmii-xmj).max(axis=1) <= r)-1 for xmii in xmi])
        print(B)
        # Similar for computing A
        m += 1
        xm = np.array([L[i:i+m] for i in range(N-m+1)])

        A = np.sum([np.sum(np.abs(xmi-xm).max(axis=1) <= r)-1 for xmi in xm])
        print(A)
        # Return SampEn
        if B == 0 or A == 0 or (A/B) > 2.72:
            response.append(1)
        else:
            response.append(-np.log(A/B))

    # 1D series, list
    return response

def main():
    print('hello')
    ts = [[1,1,1,1,1,1,1,1,14,-2,4,5,-6,-10,12,34,5,-23,-34,12,1,12,-16,23,23,2,3,0,11,-10,3.5,2,12,-12,0,-1,12,67,4,-9,34,1,1,1],[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],[0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,5,12,-23,2,4,5,-2,5,7,4,3,56,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,0.8,0.8,0.8,0.8,0.8,0.4,0.3,0.4,0.5,0.5,0.4,0.6,0.8,-0.8,-0.7,0.5,0.67,0.4,0.8,0.9],[6,9,11,12,8,3,5,3,4,2,3,-5,6,2,3,5,6,7,3,4,2,3,2,3,4,3,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,3,4,5,6,4,-5,4,8,5,6,7,3,4,2,4],[1,2,1,3,4,5,6,7,1,5,5,6,7,-5,-23,23,1,-5,4,6,-4,9,3,4,5,6,7,12,12,1,2,3,-12,1,2,3,4,5,6,24,3,5,3,4,5,4,6,5,3,4]]
        #print(0.1*np.std(ts))
    SE = sampen(ts,3,0.2)
        #print('hello')
    print (SE)
if __name__ == '__main__':
    print('Main')
    main()








#_inst = VectSampEn()
#condprob = _inst.condprob
#sampen = _inst.sampen
#qse = _inst.qse
