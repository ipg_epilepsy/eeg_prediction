# Graphic User Interface for EEG_Prediction


# Seizure prediction
from EEG_Prediction import *

# GUI
import sys
from PyQt5.QtWidgets import QApplication, QWidget

if __name__ == '__main__':
    
    app = QApplication(sys.argv)

    w = QWidget()
    w.resize(800, 800)
    w.move(300, 300)
    w.setWindowTitle('EEG')
    w.show()
    
    sys.exit(app.exec_())

