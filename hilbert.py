######################################################################
##  hilbert.py
## 
##  Applies the Hilber-Huang Transform to a multichannel EEG input
##  signal
## 
##  Project: Seizure Detection and Prediction
##  Author: Stephen Covrig
##  Date: 2018-2019
######################################################################

import numpy as np
import pyhht
import matplotlib.pyplot as plt


def get_hilbert(npz, num_imfs):

    npz_hilb = {}
    data = []

    channels = npz['data'].shape[0]

    for channel in range(channels):

        # Create an object to be used to find the intrinsic mode
        # functions of the signal
        decomposer = pyhht.EMD(npz['data'][channel])

        # Find the intrinsic mode functions of the signal
        imfs = decomposer.decompose()

        # Add channel's imfs to the data list
        data.append(imfs[0:num_imfs].transpose())

    # Convert list of channels to a 3D numpy array
    npz_hilb['data'] = np.asarray(data)

    # Copy over labels
    npz_hilb['labels'] = npz['labels']
    npz_hilb['proximity'] = npz['proximity']

    return npz_hilb


def print_hilbert(npz, channel):

    functions = npz['data'].shape[2]
    samples = npz['data'].shape[1]

    # Create image
    im = np.empty((samples, functions+2))
    im[...,0:-2] = npz['data'][channel]
    im[...,-2] = npz['labels']
    im[...,-1] = npz['proximity']

    # Create a plot
    fig, ax = plt.subplots()
    ax.imshow(im.transpose(), aspect='auto', cmap='plasma')

    # Display image of wavelet coefficients
    plt.show()


def print_hilbert_norm(npz, channel):

    functions = npz['data'].shape[2]
    samples = npz['data'].shape[1]

    data_norm = _normalize_rows(npz['data'][channel].transpose())
    proximity_norm = _normalize(npz['proximity'])

    # Create image
    im = np.empty((samples, functions+2))
    im[...,0:-2] = data_norm.transpose()
    im[...,-2] = npz['labels']
    im[...,-1] = proximity_norm

    # Create a plot
    fig, ax = plt.subplots()
    ax.imshow(im.transpose(), aspect='auto', cmap='plasma')

    # Display image of wavelet coefficients
    plt.show()


#######################
## Support functions ##
#######################

def _normalize_rows(arr):

    shape = arr.shape
    temp = np.empty(shape)

    # Normalize each row separately
    for row in range(shape[0]):
        temp[row] = _normalize(arr[row])

    return temp


def _normalize(arr):

    mi = np.amin(arr)
    ma = np.amax(arr)

    return (arr - mi)/(ma-mi)

