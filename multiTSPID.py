# Train time series prediction lstms(?) or ARIMA

from EEG_Prediction import *

from keras.models import Sequential
from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM

import numpy as np

import matplotlib.pyplot as plt

#########################################################
# Control
#########################################################

look_back = 100
batchSize = 10

#########################################################
# Functions
#########################################################

def lstm_init():
	model = Sequential()
	model.add(LSTM(1,input_shape=(1,look_back)))
	model.add(Dense(1))
	# model.add(Activation('linear'))
	model.compile(loss='mean_squared_error', optimizer='adam')
	return(model)

def singen(num=1000):
	s = np.sin(np.linspace(0,1,num)*2*np.pi)
	while True:
		for v in s:
			yield(v)

def tgen(num= 1000):
	up = np.linspace(-1,1,num/2)
	do = np.linspace(1,-1,num/2)

	while True:
		for v in up:
			yield(v)
		for v in do:
			yield(v)

def gsum(x,y,w=1):
	for a,b in zip(x,y):
		yield(w*a+(1-w)*b)

def delabeler(s,lb):
	dl = [0 for i in range(lb+1)] # delay line

	while True:
		l = dl[0]
		d = dl[1:]
		dl.insert(0,s.__next__())
		x = dl.pop()

		d = np.array(d)
		d = d[None,None,:]

		l = np.array(l)
		l = l[None]

		yield((d,l))

def delabeler_extra(s,lb):
	dl = [0 for i in range(lb+1)] # delay line

	while True:
		l = dl[0]
		d = dl[1:]
		dl.insert(0,s.__next__())
		x = dl.pop()

		d = np.array(d)
		d = d[None,None,:]

		l = np.array(l)
		l = l[None,None]

		yield((d,l))

def delabeler_w_label(s,lb):
	dl = [0 for i in range(lb+1)] # delay line

	for dat_lbl in s:
		l = dl[0]
		d = dl[1:]
		dl.insert(0,dat_lbl[0])
		x = dl.pop()

		d = np.array(d)
		d = d[None,None,:]

		l = np.array(l)
		l = l[None,None]

		yield((d,l,dat_lbl[1]))

def batcher(g,batchSize=100):
	xb = []
	yb = []
	while True:
		for i in range(batchSize):
			xb.append(g.__next__()[0])
			yb.append(g.__next__()[1])

		yield((np.array(xb),np.array(yb)))

def seizFromSession(gs,N_Channels):
	for g in gs:
		for i in range(N_Channels):
			for x,y in zip(g.data[i,:],g.label):
				if g.label:
					yield(x)

def bckgFromSession(gs,N_Channels):
	for g in gs:
		for i in range(N_Channels):
			for x,y in zip(g.data[i,:],g.label):
				if not g.label:
					yield(x)

def s_and_b_gen(sessions,N_Channels):
	for session in sessions:
		for i in range(N_Channels):
			for x,y in zip(session.data[i,:],session.label):
				yield((x,y))

def dgen(sessions,N_Channels):
	for session in sessions:
		for i in range(N_Channels):
			for x in session.data[i,:]:
				yield(x)

#########################################################
# Main
#########################################################


seiz_lstm = lstm_init()
bckg_lstm = lstm_init()

# s = singen()
# t = tgen()
# gs = [s,t]

# sm = gsum(s,t)

eeg = EEG_Prediction()
gen = eeg.generate_data()

validationSet = [gen.__next__()]

# val_gen = s_and_b_gen(validationSet,eeg.N_Channels)
val_gen = dgen(validationSet,eeg.N_Channels)

g = s_and_b_gen(gen,1)
# g = s_and_b_gen(gen,eeg.N_Channels)

dlbl_gen = delabeler_w_label(g,look_back)

ctr = 0
flip = 0
for x,y,lbl in dlbl_gen:
	if not ctr%4000:
		print('.',end='')
	if ctr > 150000:
		break
	if lbl:
		seiz_lstm.fit(x,y,verbose=0)
	else:
		bckg_lstm.fit(x,y,verbose=0)
	ctr += 1


# xy = delabeler(g,look_back)
# g = batcher(xy,batchSize=batchSize)

# model.fit_generator(g,steps_per_epoch=batchSize,use_multiprocessing=True,epochs=100)

# l = np.array([0 for i in range(look_back+1)],dtype=float)
# o = []
# od = []
# rd = []
# for i in range(2000):
# 	o = model.predict(l[None,None,1:])
# 	o = float(o)
# 	od.append(o)
# 	l = np.append(o,l)
# 	l = l[:-1]

# 	plt.plot(od)
# 	plt.show()

# genSum = gsum(s,t)
# gs.append(genSum)

# resultsAcc = []
# resultsMse = []
# test with sin wav

r_list = []
for m in [seiz_lstm,bckg_lstm]:
	resultsMse = []
	# for g in gs:
	xy = delabeler(val_gen,look_back)
	for i in range(validationSet[0].data.shape[1]-1000):
		x,y = xy.__next__()
		mse = m.evaluate(x,y,verbose=0)
		# resultsAcc.append(acc)
		resultsMse.append(mse)
	r_list.append(resultsMse)
# print(resultsMse)
labels = validationSet[0].label

seiz_mse = np.array(r_list[0])
bckg_mse = np.array(r_list[1])

diff_mse = bckg_mse - seiz_mse # if positive, seiz mse is lower, indicating 1

bi_diff = []
for m in diff_mse:
	if m>0:
		bi_diff.append(1)
	else:
		bi_diff.append(0)

# how about instead, we normalize the values.
# perhaps they have trained unevenly.

# use sparsity to my advantage
# we can find the probability of a sequence occurring
# and use that to fit it to a classification

# the more the predictor has been exposed to a pattern,
# the better it will become at predictng that mode

# the brain is composed of hidden architectures
# and subsystems

# these subsystems produce electrical signalling





plt.plot(bi_diff)
plt.plot(validationSet[0].label)
plt.show()





# then triangle

# then with alternating mix

# https://www.youtube.com/watch?v=I-c49uqEXtE