from EEG_Prediction import *

e = EEG_Prediction()
dg = e.generate_data()

for d in dg:
# d = dg.__next__()

	for chn in range(len(d.bins)):

		n = d.bins[chn]

		for i in range(n.shape[0]):
			n[i,:] -= n[i,:].min()
			n[i,:] = n[i,:]/n[i,:].max()

		# n = (n-n.min())/n.max()


		# plt.imshow(d.bins[0],aspect='auto',cmap='plasma')
		# plt.imshow(n,aspect='auto',cmap='plasma')
		# plt.show()

		# t = .5
		t = n.mean() + n.var()*2

		n[n>t] = 1
		n[n<=t] = 0
		lbl = np.array(d.time_frequency_labels)
		lbl = lbl[None,:]
		lbl -= lbl.min()
		lbl = lbl/lbl.max()
		n = np.append(n,lbl,axis=0)

		plt.imshow(n,aspect='auto',cmap='plasma')
		plt.show()



