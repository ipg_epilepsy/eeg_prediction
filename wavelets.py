######################################################################
##  wavelets.py
## 
##  Applies the Discrete Wavelet Transform to a multichannel EEG input
##  signal
## 
##  Project: Seizure Detection and Prediction
##  Author: Stephen Covrig
##  Date: 2018-2019
######################################################################

import numpy as np
import pywt
import matplotlib.pyplot as plt


#########################################################
## Use the following code with an array data structure ##
#########################################################

def get_wavelets(npz, wave_type, level):

    npz_waves = {}
    data = []

    channels = npz['data'].shape[0]

    for channel in range(channels):

        # Compute wavelet coefficients of channel
        coef = pywt.wavedec(
                npz['data'][channel,], 
                wave_type, 
                'symmetric', 
                level)

        # Fix unwanted errors with the PyWavelet function
        coef, num_trimmed = _trim_coefficients(coef)

        # Expand scaling coefficient array to the length 
        # of the last decomposition level
        coef[0] = np.repeat(coef[0], 2**(level-1))

        for i in range(1, level+1):

            # Expand wavelet coefficient array to the length 
            # of the last decomposition level
            coef[i] = np.repeat(coef[i], 2**(level-i))

        # Convert list of coefficients to 2D numpy array
        coef = np.asarray(coef)

        # Transpose array to collect all coefficients corresponding
        # with a specific moment in time into a single array
        coef = coef.transpose()

        # Add channel to list
        data.append(coef)

    # Convert list of channels to a 3D numpy array
    npz_waves['data'] = np.asarray(data)

    # Get the number of samples in each channel
    samples = npz_waves['data'].shape[1]

    # Adjust upper limit to accomidate for coefficient trimming
    label_size = npz['labels'].size
    upper_limit = int(label_size-(label_size*num_trimmed/float(samples)))

    # Shrink the labelling and proximity array to match the length of the
    # last decomposition level
    npz_waves['labels'] = _decimate_labels(npz['labels'][0:upper_limit], samples)
    npz_waves['proximity'] = _decimate_labels(npz['proximity'][0:upper_limit], samples)

    return npz_waves


def print_wavelets(npz, channel):

    level = npz['data'].shape[2]
    samples = npz['data'].shape[1]

    # Create image
    im = np.empty((samples, level+2))
    im[...,0:-2] = npz['data'][channel]
    im[...,-2] = npz['labels']
    im[...,-1] = npz['proximity']

    # Create a plot
    fig, ax = plt.subplots()
    ax.imshow(im.transpose(), aspect='auto', cmap='plasma')

    # Display image of wavelet coefficients
    plt.show()


def print_wavelets_norm(npz, channel):

    level = npz['data'].shape[2]
    samples = npz['data'].shape[1]

    data_norm = _normalize_rows(npz['data'][channel].transpose())
    proximity_norm = _normalize(npz['proximity'])

    # Create image
    im = np.empty((samples, level+2))
    im[...,0:-2] = data_norm.transpose()
    im[...,-2] = npz['labels']
    im[...,-1] = proximity_norm

    # Create a plot
    fig, ax = plt.subplots()
    ax.imshow(im.transpose(), aspect='auto', cmap='plasma')

    # Display image of wavelet coefficients
    plt.show()


################################################################
## Use the following code with a dictionary storage structure ##
################################################################

def get_wavelets_old(data, wave_type, level):

    # Initialize list
    wavelets = {}

    for channel in data:
        
        # Skip labelling information
        if channel != 'labels':

            # Compute wavelet coefficients of channel
            coef = pywt.wavedec(data[channel], wave_type, 'periodization', level)

            # Fix unwanted errors with the PyWavelet function
            coef, num_trimmed = _trim_coefficients(coef)

            # Expand scaling coefficient array to the length 
            # of the last decompositionn level
            coef[0] = np.repeat(coef[0], 2**(level-1))

            for i in range(1, level+1):

                # Expand wavelet coefficient array to the length 
                # of the last decompositoin level
                coef[i] = np.repeat(coef[i], 2**(level-i))

            # Convert list of coefficients to 2D numpy array
            coef = np.asarray(coef)

            # Transpose array to collect all coefficients corresponding
            # with a specific moment in time into a single array
            coef = coef.transpose()

            # Add channel to list
            wavelets[channel] = coef

    # Get the length of a channel
    arb_channel = wavelets.keys()[0]
    coef_size = wavelets[arb_channel].shape[0]

    # Adjust upper limit to accomidate for coefficient trimming
    label_size = data['labels'].size
    upper_limit = label_size-(label_size*num_trimmed/coef_size)

    # Shrink the labelling array to match the length of the
    # last decomposition level
    wavelets['labels'] = _decimate_labels(data['labels'][0:upper_limit], coef_size)

    return wavelets


def print_wavelets_old(channel, labels):

    level = channel.shape[1]

    # Create a plot
    fig, axs = plt.subplots(nrows=level+1)
    fig.subplots_adjust(wspace=0, hspace=0)

    for i, ax in zip(range(0, level), axs.flat):

        # Format subplot for wavelet coefficients
        ax.imshow(np.expand_dims(channel.transpose()[i], axis=0), aspect='auto', cmap='plasma')
        ax.axis('off')

    # Display labels
    axs[-1].imshow(np.expand_dims(labels, axis=0), aspect='auto', cmap='plasma')
    axs[-1].axis('off')

    # Display image of wavelet coefficients
    plt.show()


def print_wavelets_norm_old(channel, labels):

    level = channel.shape[1]

    # Create image
    norm = _normalize_rows(channel.transpose())
    t_labels = np.expand_dims(labels, axis=0)
    im = np.concatenate((norm, t_labels), axis=0)

    # Create a plot
    fig, ax = plt.subplots()
    ax.imshow(im, aspect='auto', cmap='plasma')

    # Display image of wavelet coefficients
    plt.show()


#######################
## Support functions ##
#######################

def _decimate_labels(labels, new_size):

    temp = np.empty(new_size)
    old_size = labels.size

    for i in range(new_size):
        temp[i] = labels[int(old_size*i/new_size)]

    return temp


def _trim_coefficients(coef):

    levels = len(coef)

    # Get length of scaling coefficient array
    n_scaling = coef[0].size

    # Get length of max decomp level array
    n_max = coef[-1].size

    # Determine the expected multiplier between
    # the two lengths
    multiplier = 2**(levels-2)

    # Decrease scaling coefficient length and 
    # trim all other coefficient lengths
    if n_max < n_scaling*multiplier:

        while n_max < n_scaling*multiplier:
            n_scaling = n_scaling-1

        coef[0] = coef[0][0:n_scaling]

        for i in range(1, levels):
            coef[i] = coef[i][0:n_scaling*(2**(i-1))]

    # Leave scaling coefficient length the same and
    # trim all other coefficient lengths
    elif n_max > n_scaling*multiplier:

        for i in range(1, levels):
            coef[i] = coef[i][0:n_scaling*(2**(i-1))]

    n_new_max = coef[-1].size

    return coef, n_max-n_new_max


def _normalize_rows(arr):

    shape = arr.shape
    temp = np.empty(shape)

    # Normalize each row separately
    for row in range(shape[0]):
        temp[row] = _normalize(arr[row])

    return temp


def _normalize(arr):

    mi = np.amin(arr)
    ma = np.amax(arr)

    return (arr - mi)/(ma-mi)


def _compute_divisor(number):

    if number is 1:
        return 2
    else:
        return 2**(number-1) + _compute_divisor(number-1)


